import urllib3
import json
import locale
import time
import dateutil.parser
from datetime import datetime, timedelta

http = urllib3.PoolManager()
locale.setlocale(locale.LC_ALL, '')

organization = "MICHIGAN STATE UNIVERSITY"

def queryNIH(offset):

    headers = {'Content-type': 'application/json; charset=utf-8'}

    body = '  {\
            "criteria":\
            {\
                "org_names": ["' + organization + '"]\
            },\
            "include_fields": [\
                 "ApplId","SubprojectId","FiscalYear","Organization", "ProjectNum",\
                 "ProjectNumSplit","ContactPiName","AllText","FullStudySection",\
                 "ProjectStartDate","ProjectEndDate","DeptType"\
             ],\
             "offset":0,\
             "limit":50,\
             "sort_field":"project_start_date",\
              "sort_order":"desc"\
       }'   # CHANGE offset TO offset VARIABLE TO RUN RECURSIVELY 

    f = r"https://api.reporter.nih.gov/v2/projects/search"
    print("Querying: " + f + " [offset: " + offset +"] \n")
    r = http.request('POST', f, body=body, headers=headers)
    jsonData = json.loads(r.data)
    return jsonData

def jsonDataPrint(jsonData):
    search_id = str(jsonData["meta"]["search_id"])

    for i in jsonData["results"]:

        output = "appl_id: " + str(i["appl_id"]) + "\n"
        output += "details_url: " + "https://reporter.nih.gov/search/" + str(search_id) + "/project-details/" + str(i["appl_id"]) + "\n"
        output += "subproject_id: " + str(i["subproject_id"]) + "\n"
        output += "fiscal_year: " + str(i["fiscal_year"]) + "\n"
        output += "award_num: " + str(i["project_num"]) + "\n"
        if (i["full_study_section"]):
            output += "name: " + i["full_study_section"]["name"] + "\n"
        else:
            output += "name: \n"
        output += "org_name: " + i["organization"]["org_name"] + "\n"
        output += "org_city: " + i["organization"]["org_city"] + "\n"
        output += "org_country: " + i["organization"]["org_country"] + "\n"
        output += "org_state: " + i["organization"]["org_state"] + "\n"
        output += "contact_pi_name: " + str(i["contact_pi_name"]) + "\n"
        output += "dept_type: " + str(i["organization"]["dept_type"]) + "\n"
        output += "project_start_date: " + str(i["project_start_date"]) + "\n"
        output += "project_end_date: " + str(i["project_end_date"]) + "\n"
        output += "\n------------------------\n\n"
        
        parsedStartDate = dateutil.parser.parse(str(i["project_start_date"]))        
        today = datetime.now()
        twoWeeksAgo = today - timedelta(days=14)

        if (str(parsedStartDate) >= str(twoWeeksAgo)):
            print(output)

page = 1
jsonData = queryNIH(str(page))
printData = jsonDataPrint(jsonData) # COMMENT THIS LINE TO RUN RECURSIVELY

# UNCOMMENT THIS BLOCK TO RUN RECURSIVELY
# while (jsonData != 0):    
#     printData = jsonDataPrint(jsonData)
#     page = page + 50
#     time.sleep(5)
#     jsonData = queryNIH(str(page))
