# Query NIH for recent awards

Script that queries the National Institutes of Health API (NIH RePORTER) for recent awards.  

This script searches NIH by organization, for any awards that were issued in the past N days (default is 14 because they claim to update the data weekly).  You can find examples of other JSON payloads at the website below.  The final destination for this code will either be AWS Lambda or GCP Cloud Functions (haven't decided yet).  

https://api.reporter.nih.gov/

There is also a version of the code for searching recursively, but keep in mind that NIH gets grumpy if you do that too often.

Written in Python 3.9.5  
